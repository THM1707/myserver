package com.thm.app_server.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.thm.app_server.model.Role;
import com.thm.app_server.model.RoleName;

public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(RoleName name);
}
